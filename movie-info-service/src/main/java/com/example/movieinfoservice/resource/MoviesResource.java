package com.example.movieinfoservice.resource;

import com.example.movieinfoservice.model.Movie;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/movies")
public class MoviesResource {

    @GetMapping("/{movieId}")
    public Movie getMovie(@PathVariable("movieId") String movieId) throws InterruptedException {
        return new Movie(movieId, "Monster University", "Great animation movie, the Monsters Inc. sequel");
    }
}
