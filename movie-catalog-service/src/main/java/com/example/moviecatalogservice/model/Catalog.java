package com.example.moviecatalogservice.model;

import java.util.List;

public class Catalog {
    private String userId;
    private List<CatalogItem> catalogItems;

    public Catalog(String userId, List<CatalogItem> catalogItems) {
        this.userId = userId;
        this.catalogItems = catalogItems;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<CatalogItem> getCatalogItems() {
        return catalogItems;
    }

    public void setCatalogItems(List<CatalogItem> catalogItems) {
        this.catalogItems = catalogItems;
    }
}
