package com.example.moviecatalogservice;

import com.example.moviecatalogservice.service.MovieInfoService;
import com.example.moviecatalogservice.service.RatingsDataService;
import com.example.moviecatalogservice.service.impl.ApiBasedMovieInfoService;
import com.example.moviecatalogservice.service.impl.ApiBasedRatingsDataService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableCircuitBreaker
@EnableHystrixDashboard
public class MovieCatalogServiceApplication {
	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public RatingsDataService ratingsDataService() {
		return new ApiBasedRatingsDataService(restTemplate());
	}

	@Bean
	public MovieInfoService movieInfoService() {
		return new ApiBasedMovieInfoService(restTemplate());
	}

	public static void main(String[] args) {
		SpringApplication.run(MovieCatalogServiceApplication.class, args);
	}

}
