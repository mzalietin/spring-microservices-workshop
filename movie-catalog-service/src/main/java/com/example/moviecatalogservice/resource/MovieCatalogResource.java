package com.example.moviecatalogservice.resource;

import com.example.moviecatalogservice.model.*;
import com.example.moviecatalogservice.service.MovieInfoService;
import com.example.moviecatalogservice.service.RatingsDataService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/catalog")
public class MovieCatalogResource {
    @Autowired
    private RatingsDataService ratingsDataService;
    @Autowired
    private MovieInfoService movieInfoService;

    @GetMapping("/{userId}")
    public Catalog getCatalog(@PathVariable("userId") String userId) {
        UserRating userRating = ratingsDataService.getUserRating(userId);

        List<CatalogItem> items = userRating.getRatings().stream()
            .map(this::getCatalogItem)
            .collect(Collectors.toList());

        return new Catalog(userId, items);
    }

    private CatalogItem getCatalogItem(Rating r) {
        Movie m = movieInfoService.getMovie(r.getMovieId());
        return new CatalogItem(m.getMovieName(), m.getMovieDescription(), r.getRating());
    }

    public void setRatingsDataService(RatingsDataService ratingsDataService) {
        this.ratingsDataService = ratingsDataService;
    }

    public void setMovieInfoService(MovieInfoService movieInfoService) {
        this.movieInfoService = movieInfoService;
    }
}
