package com.example.moviecatalogservice.service.impl;

import com.example.moviecatalogservice.model.Movie;
import com.example.moviecatalogservice.service.MovieInfoService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

public class ApiBasedMovieInfoService implements MovieInfoService {
    @Autowired
    private final RestTemplate restTemplate;

    public ApiBasedMovieInfoService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    @HystrixCommand(fallbackMethod = "getFallbackMovie",
        commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "2000"),
            @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
            @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
            @HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "5000")
        }
    )
    public Movie getMovie(String movieId) {
        return restTemplate.getForObject("http://movie-info-service/movies/" + movieId, Movie.class);
    }

    private Movie getFallbackMovie(String movieId) {
        return new Movie(movieId, "Not found", "");
    }
}
