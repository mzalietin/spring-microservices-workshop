package com.example.moviecatalogservice.service.impl;

import com.example.moviecatalogservice.model.Rating;
import com.example.moviecatalogservice.model.UserRating;
import com.example.moviecatalogservice.service.RatingsDataService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

public class ApiBasedRatingsDataService implements RatingsDataService {
    @Autowired
    private final RestTemplate restTemplate;

    public ApiBasedRatingsDataService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    @HystrixCommand(fallbackMethod = "getFallbackUserRating")
    public UserRating getUserRating(String userId) {
        return restTemplate.getForObject("http://ratings-data-service/ratings/" + userId, UserRating.class);
    }

    private UserRating getFallbackUserRating(String userId) {
        return new UserRating(userId, Collections.singletonList(new Rating("0", 0)));
    }
}
