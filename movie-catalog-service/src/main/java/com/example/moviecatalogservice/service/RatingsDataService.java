package com.example.moviecatalogservice.service;

import com.example.moviecatalogservice.model.UserRating;

public interface RatingsDataService {
    UserRating getUserRating(String userId);
}
