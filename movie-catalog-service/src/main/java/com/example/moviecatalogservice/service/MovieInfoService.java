package com.example.moviecatalogservice.service;

import com.example.moviecatalogservice.model.Movie;

public interface MovieInfoService {
    Movie getMovie(String movieId);
}
