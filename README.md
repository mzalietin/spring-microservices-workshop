## Personal movie catalog
Project's purpose is microservices architecture research, so you may find some stuff hardcoded :)

### Services
Movies API:

+ localhost:8282/movies/{movieId}

User rating API:

+ localhost:8383/ratings/{userId}
+ localhost:8383/ratings/{userId}/{movieId}

Movie catalog API:

+ localhost:8181/catalog/{userId}

Monitoring:

+ localhost:8181/hystrix
