package com.example.ratingsdataservice.resource;

import com.example.ratingsdataservice.model.Rating;
import com.example.ratingsdataservice.model.UserRating;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/ratings")
public class RatingsResource {

    @GetMapping("/{userId}/{movieId}")
    public Rating getRating(@PathVariable("userId") String userId, @PathVariable("movieId") String movieId) {
        return new Rating(movieId, 10);
    }

    @GetMapping("/{userId}")
    public UserRating getRatingsOfUser(@PathVariable("userId") String userId) {
        return new UserRating(userId, Arrays.asList(
                new Rating("1234", 7),
                new Rating("4567", 9),
                new Rating("7890", 10)
        ));
    }
}
